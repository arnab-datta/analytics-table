import React from 'react';
import { render, screen } from '@testing-library/react';
import AnalyticsTable from './AnalyticsTable';

test('renders learn react link', () => {
  render(<AnalyticsTable />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
