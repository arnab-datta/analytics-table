// Built-in
import React from 'react';
import { HashRouter as Router, Route, Routes } from "react-router-dom";

// Internal
import './styles/css/AnalyticsTable.css';
import Analytics from "./Pages/Analytics/Analytics";
import NotFoundPage from "./Pages/404/404";

function AnalyticsTable() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Analytics/>} />
        <Route element={<NotFoundPage />} />
      </Routes>
    </Router>
  );
}

export default AnalyticsTable;
