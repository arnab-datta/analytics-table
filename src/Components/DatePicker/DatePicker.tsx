import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons'
import '../../styles/css/DatePicker.css';

function DatePicker(props:any){
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');

    const startDateHandler = (e:any) =>{
            setStartDate(e.target.value);
            props.setStartDateSelected(e.target.value);
    }
    const endDateHandler = (e:any) =>{
        setEndDate(e.target.value);
        props.setEndDateSelected(e.target.value);
        if(startDate !== ""){
            props.setIsLoading(false)
        }
        
    }
    return(
    <div className="datepicker__wrapper d__flex al__it__cen jus__con__sp__ev us_none">
        <div className="calendar center__flex"><FontAwesomeIcon icon={faCalendarAlt} /></div>
        <div className='date_range_value d__flex jus__con__sp__ev al__it__cen'>
            <div className="dtShow center__flex btn-wrp">
                <input type="date" 
                min="2021-06-01"
                max="2021-07-31"
                onChange={startDateHandler}
                />
            </div>
            <span className="divider center__flex">-</span>
            <div className="dtShow center__flex btn-wrp">
                <input type="date"
                  min="2021-06-01"
                  max="2021-07-31"
                 onChange={endDateHandler}
                />
            </div>
        </div>
       
       
    </div>
    );
}
export default DatePicker;

