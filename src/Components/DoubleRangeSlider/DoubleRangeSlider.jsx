import { useEffect, useState, useRef } from "react";
import "../../styles/css/DoubleRangeSlider.css";

function DoubleRangeSlider(props) {
  const [sliderOneVal, setSliderOneVal] = useState(props.filterRangeStart);
  const [sliderTwoVal, setSliderTwoVal] = useState(props.filterRangeEnd);

  const [viewValue1, setViewValue1] = useState(props.filterRangeStart);
  const [viewValue2, setViewValue2] = useState(props.filterRangeEnd);

  const [minValue] = useState(props.rangeStartVal);
  const [maxValue] = useState(props.rangeEndVal);

  const minGap = useState(0);
  const sliderTrack = useRef("");

  useEffect(() => {
    slideOneFunc();
    slideTwoFunc();
  }, []);

  const slideOneFunc = () => {
    if (sliderTwoVal - sliderOneVal <= minGap) {
      const sliderOneVal = sliderTwoVal - minGap;
      setSliderOneVal(sliderOneVal);
    }
    setViewValue1(sliderOneVal);
    fillSliderColor();
  };
  const slideTwoFunc = () => {
    if (sliderTwoVal - sliderOneVal <= minGap) {
      const sliderTwoVal = sliderOneVal + minGap;
      setSliderTwoVal(sliderTwoVal);
    }
    setViewValue2(sliderTwoVal);
    fillSliderColor();
  };
  const slideOneHandler = (e) => {
    const _this = e.target;
    const val1 = _this.value;
    if (sliderTwoVal - val1 <= minGap) {
      const sliderOneVal = sliderTwoVal - minGap;
      setSliderOneVal(sliderOneVal);
    }
    setViewValue1(val1);
    props.setRangeStartVal(val1);
    fillSliderColor();
  };
  const slideTwoHandler = (e) => {
    const _this = e.target;
    const val2 = _this.value;
    if (val2 - sliderOneVal <= minGap) {
      const sliderTwoVal = sliderOneVal + minGap;
      setSliderTwoVal(sliderTwoVal);
    }
    setViewValue2(val2);
    props.setRangeEndVal(val2);
    fillSliderColor();
  };
  const fillSliderColor = () => {
    const min = minValue;
    const max = maxValue;
    const percent1 = ((viewValue1 - min) / (max - min)) * 100;
    const percent2 = ((viewValue2 - min) / (max - min)) * 100;
    sliderTrack.current.style.backgroundImage = `linear-gradient(to right, #dadae5 ${percent1}% , #3264fe ${percent1}% , #3264fe ${percent2}%, #dadae5 ${percent2}%)`;
  };

  return (
    <>
      <div className="range__wrapper">
        <div className="container__cls">
          <div className="slider__tracker" ref={sliderTrack}></div>
          <input
            type="range"
            id="slider-1"
            min={minValue}
            max={maxValue}
            defaultValue={sliderOneVal}
            onChange={slideOneHandler}
          />
          <input
            type="range"
            id="slider-2"
            min={minValue}
            max={maxValue}
            defaultValue={sliderTwoVal}
            onChange={slideTwoHandler}
          />
        </div>
        <div className="values">
          <span id="viewvalue1">{viewValue1}</span>
          <span id="viewvalue2">{viewValue2}</span>
        </div>
      </div>
    </>
  );
}

export default DoubleRangeSlider;
