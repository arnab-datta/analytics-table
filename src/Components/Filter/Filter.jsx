import DoubleRangeSlider from "../DoubleRangeSlider/DoubleRangeSlider";
import SearchBarTextFilter from "../SearchBarTextFilter/SearchBarTextFilter";
import "../../styles/css/filter_popup.css";
import { useEffect, useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons'
import moment from "moment";

function Filter(props) {
  const [rangeStart, setRangeStart] = useState(0);
  const [rangeEnd, setRangeEnd] = useState(0);
  const [filterRangeStart, setFilterRangeStart] = useState(0);
  const [filterRangeEnd, setFilterRangeEnd] = useState(0);
  const [isEnabled, setIsEnabled] = useState(true);
  const [textFilter, setTextFilter] = useState([]);
  const [dateFilter, setDateFilter] = useState([]);
 

  const resetHandler = (ev) => {
    const tableHeadings = JSON.parse(JSON.stringify(props.tableHeadingsVal)); // deep copy
    const fiH = tableHeadings.findIndex(
      (el) =>
        el.colshort.trim().toLowerCase() ===
        props.col_obj.colshort.trim().toLowerCase()
    );
    if (fiH > -1) {
      tableHeadings[fiH].filterShow = false;
      tableHeadings[fiH].fieldAllValues = []
      tableHeadings[fiH].fieldFilterValues = []
      props.setTableHeadingsVal(JSON.parse(JSON.stringify(tableHeadings)));
    }
    setTextFilter([])
    setRangeStart(0)
    setRangeEnd(0)
    let tMainData = JSON.parse(JSON.stringify(props.TableMainData));
    props.setTableData(tMainData);
  };
  const applyHandler = (ev) => {

    const tableHeadings = JSON.parse(JSON.stringify(props.tableHeadingsVal)); // deep copy
    const fiTHead = tableHeadings.findIndex(
      (el) =>
        el.colshort.trim().toLowerCase() ===
        props.col_obj.colshort.trim().toLowerCase()
    );

    const col_short = props.col_obj.colshort;
    // let tData = JSON.parse(JSON.stringify(props.TableMainData)); // work as OR  filters
    let tData = JSON.parse(JSON.stringify(props.TableData));  // work as AND  filters
    let fi = tData[0].findIndex(col => col.colshort.trim().toLowerCase() === col_short);
    let tDataFiltered = [];

    switch(props.col_obj.fieldType )
    {
      case "number" : 
                {
                  if (
                    (rangeStart !== 0 &&  rangeEnd !== 0 ) &&
                    (rangeStart !== props.filterValues[0] && rangeEnd !== props.filterValues[1])
                  ) {
                    tDataFiltered = tData.filter(
                      (row) =>
                        row[fi].value > Number(rangeStart) &&
                        row[fi].value < Number(rangeEnd)
                    );
                    tDataFiltered = tDataFiltered.filter(row => row.length !== 0);
                    props.setTableData(tDataFiltered);
                    if (fiTHead > -1) {
                      tableHeadings[fiTHead].fieldFilterValues = [rangeStart, rangeEnd]
                    }
                  }
                  else{
                    const strt = props.filterValues[0] 
                    const end = props.filterValues[1]
                    tDataFiltered = tData.filter(
                      (row) =>
                        row[fi].value >= Number(strt) && row[fi].value <= Number(end)
                    );
                    tDataFiltered = tDataFiltered.filter((row) => row.length !== 0);
                    props.setTableData(tDataFiltered);
                  }
                  break;
                }
      case "string" : 
                {
                  if(textFilter.length > 0)
                  {
                    tDataFiltered = tData.filter(row => textFilter.includes(row[fi].value));
                    tDataFiltered = tDataFiltered.filter((row) => row.length !== 0);
                    props.setTableData(tDataFiltered);
                    if (fiTHead > -1) {
                      tableHeadings[fiTHead].fieldFilterValues = JSON.parse(JSON.stringify(textFilter))
                    }
                  }
                  break;
                }
      case "date" : 
                {
                  const dF = JSON.parse(JSON.stringify(dateFilter))
                  tDataFiltered = tData.filter(row => 
                    moment(new Date(row[fi].value)).format("YYYY-MM-DD") >= dF[0] 
                    &&  moment(new Date(row[fi].value)).format("YYYY-MM-DD") <= dF[1]
                  );
                  tDataFiltered = tDataFiltered.filter((row) => row.length !== 0);
                  props.setTableData(tDataFiltered);
                  if (fiTHead > -1) {
                    tableHeadings[fiTHead].fieldFilterValues = JSON.parse(JSON.stringify(dF))
                  }
                }
    }

    /*******  close the filter **************/
    if (fiTHead > -1) {
      tableHeadings[fiTHead].filterShow = false;
      props.setTableHeadingsVal(JSON.parse(JSON.stringify(tableHeadings)));
    }
  };

  return (
    <>
      {/* {props.openFilter && */}
      <div
        className="filter_popup"
        style={{
          left: props.pos_lft,
          right: props.pos_rgt,
        }}
      >
        <div className="filter_popup_inner">
          <h3>{props.col_obj.colname}</h3>
          {props.col_obj.fieldType === "string" && (
            <SearchBarTextFilter
              AllValues={props.AllValues}
              filterValues={props.filterValues}
              setIsEnabled={setIsEnabled}
              setTextFilterValue={setTextFilter}
            />
          )}

          {props.col_obj.fieldType === "number" && (
            <div>
              <DoubleRangeSlider
                rangeStartVal={props.AllValues[0]}
                rangeEndVal={props.AllValues[1]}
                filterRangeStart={props.filterValues[0]}
                filterRangeEnd={props.filterValues[1]}
                setRangeStartVal={setRangeStart}
                setRangeEndVal={setRangeEnd}
              ></DoubleRangeSlider>
            </div>
          )}

          {props.col_obj.fieldType === "date" && (
            <DateFilter
              AllValues={props.AllValues}
              filterValues={props.filterValues}
              setIsEnabled={setIsEnabled}
              setDateFilterValue={setDateFilter}
            />
          )}
          <div className="btn__group__wrapper">
            <button
              onClick={resetHandler}
              className={`btn__group reset__filter__btn center__flex cu__pnt ${
                !isEnabled ? "disabled" : ""
              }`}
              disabled={!isEnabled}
            >
              Reset
            </button>
            <button
              onClick={applyHandler}
              className={`btn__group apply__filter__btn center__flex cu__pnt ${
                !isEnabled ? "disabled" : ""
              }`}
              disabled={!isEnabled}
            >
              Apply
            </button>
          </div>
        </div>
      </div>

      {/* } */}
    </>
  );
}
export default Filter;

const DateFilter = (props) => {
  const minDate = props.AllValues[0]
  const maxDate = props.AllValues[1]

  const [startDateFilter, setStartDateFilter] = useState(props.filterValues[0]);
  const [endDateFilter, setEndDateFilter] = useState(props.filterValues[1]);

  const [dates,setDates] = useState([]); 

  const startDateFilterHandler = (e) =>{
    // setStartDateFilter(e.target.value);
    let arr = JSON.parse(JSON.stringify(dates));
    arr[0] = e.target.value;
    setDates(arr)
    props.setDateFilterValue(arr);
    // props.setStartDateSelected(e.target.value);
  }
  const endDateFilterHandler = (e) =>{
    let arr = JSON.parse(JSON.stringify(dates));
    arr[1] = e.target.value;
    setDates(arr)
    props.setDateFilterValue(arr);
    // setEndDateFilter(e.target.value);
    // props.setDateFilterValue()
      // props.setEndDateSelected(e.target.value);
      // if(startDate !== ""){
      //     props.setIsLoading(false)
      // }
      
  }
  return (
    <>
       <div className="datepicker__wrapper d__flex al__it__cen jus__con__sp__ev us_none">
        <div className="calendar center__flex"><FontAwesomeIcon icon={faCalendarAlt} /></div>
        <div className='date_range_value d__flex jus__con__sp__ev al__it__cen'>
            <div className="dtShow center__flex btn-wrp">
                <input type="date" 
                min={minDate}
                max={maxDate}
                value={startDateFilter}
                onChange={startDateFilterHandler}
                />
            </div>
            <span className="divider center__flex">-</span>
            <div className="dtShow center__flex btn-wrp">
                <input type="date"
                value={endDateFilter}
                 min={minDate}
                 max={maxDate}
                 onChange={endDateFilterHandler}
                />
            </div>
        </div>
    </div>
    </>
  );
}

