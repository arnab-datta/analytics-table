import "../../styles/css/Loader.css";

function Loader() {
  return (
    <div className="loader__wrapper">
      <div className="loader"></div>
    </div>
  );
}

export default Loader;
