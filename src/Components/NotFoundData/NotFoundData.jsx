
import "../../styles/css/notfounddata.css";
import data_not_found from "../../assets/images/data_not_found.svg";

function NotFoundData(){
    return(
        <>
        <div className="not__found__wrapper center__flex">
            <div className="not__found__inner">
                <div className="leftImg" >
                    <img src={data_not_found} alt="df" />
                </div>
                <div className="rightNotF">
                    <h2>Hey! Something’s off! We couldn’t display the given data.</h2>
                    <p>Try changing your your filters or selecting a different date.</p>
                </div>
            </div>
        </div>
        </>
    )
}
export default NotFoundData;