import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faTimes } from "@fortawesome/free-solid-svg-icons";
import { useEffect, useState } from "react";
import "../../styles/css/SearchBarTextFilter.css";

function SearchBarTextFilter(props) {
    // const viewDataArrAll = props.AllValues.filter((v, i, a) => a.indexOf(v) === i);
    // viewDataArrAll.sort();
    // const viewDataArrFilter = props.filterValues.filter((v, i, a) => a.indexOf(v) === i);
    // viewDataArrFilter.sort();

    const [allData, setAllData] = useState([]);
    const [filterData, setFilterdData] = useState([]);

    const [searchValue, setSearchValue] = useState("");
    const [showCross, setShowCross] = useState(false);
    const [valuesSelected, setValuesSelected] = useState([]);

    useEffect(() => {
          const allList = JSON.parse(JSON.stringify(props.AllValues))
          const filterList = JSON.parse(JSON.stringify(props.filterValues))
          setAllData(allList)
          setFilterdData(filterList)
    },[])
    const handleFilter = (e) => {
      const searchWord = e.target.value.trim().toLowerCase();
      setSearchValue(searchWord);
      const newFilter = allData.filter((value) =>
        value.trim().toLowerCase().includes(searchWord)
      );
      setAllData(newFilter);
      if (allData.length === 0) 
      {
        props.setIsEnabled(false)
        setShowCross(true)
      }
      else 
      {
        props.setIsEnabled(true)
        setShowCross(false)
      }
    };

    const clearTextHandler = (e) => {
      setSearchValue("");
      setAllData(JSON.parse(JSON.stringify(allData)))
      props.setIsEnabled(true)
      setShowCross(false)
    };

    const multiValueSelection = (e)  => {
        const _this = e.target;
        let data = JSON.parse(JSON.stringify(valuesSelected));
        if(data.includes(_this.value))
        {
          data = data.filter(el => el != _this.value)
        }
        else
        {
          data.push(_this.value);
        }
        setValuesSelected(data);
        setFilterdData(data);
        if(data.length > 0)
        {
          props.setTextFilterValue(data);
        }
    }
    return (
      <>
        <div className="search">
          <div className="searchInputs">
            <div className="searchIcon center__flex">
              <FontAwesomeIcon icon={faSearch} />
            </div>
            <input
              type="text"
              className="searchBox"
              placeholder="Search"
              value={searchValue}
              onChange={handleFilter}
            />
            <div className={`clearTxtIcon center__flex ${!showCross ? 'vishidden' : ''}`}>
              <FontAwesomeIcon icon={faTimes}
              onClick={clearTextHandler}
              />
            </div>
          </div>
          {allData.length !== 0 && (
            <div className="searchResults">
              {allData.map((value, index) => {
                return (
                  <div  key={index} className="dataItem">
                    <input type="checkbox" value={value} id={`v_${index}`}
                    checked={filterData.includes(value)}
                    onChange={multiValueSelection}
                    />
                    <label htmlFor={`v_${index}`} className="itemTxt">{value}</label>
                  </div>
                  
                );
              })}
            </div>
          )}
        </div>
      </>
    );
  }

  
  export default SearchBarTextFilter;