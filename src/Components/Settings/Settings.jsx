import { useEffect, useState } from 'react';
import "../../styles/css/Settings.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSlidersH } from "@fortawesome/free-solid-svg-icons";

function Settings(props) {

  const [openSettings, setOpenSettings] = useState(false);
  const [isDisabled, setDisabled] = useState(false);
  const [columnsCardsHistory,setColumnsCardsHistory] = useState([]);
  const [columnsCards,setColumnsCards] = useState([]);
  const [draggedAndMovedItems, setDraggedAndMovedItems] = useState([]);

  const  arraymove = (arr, fromIndex, toIndex) => {
    var element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);
  }

  useEffect(() => {
    const arr = JSON.parse(JSON.stringify(props.colNames));
    const cardsColumnList = [...columnsCards];
    arr.forEach((el,i) => {
        const col = {
          colshort : el.colshort,
          colname : el.colname,
          selected : el.isVisible,
          order : el.order
        }
        cardsColumnList.push(col);
    });
    setColumnsCards(cardsColumnList);
  },[])

  const tableSettingsHandler = (e) => {
    setOpenSettings(true)
    setDisabled(true);
    const cardsColumnListHistory = JSON.parse(JSON.stringify(columnsCards)) // deep copy
    setColumnsCardsHistory(cardsColumnListHistory);
   
    
  };


  const closeSettingsHandler = function (e) {
    setOpenSettings(false);
    setDisabled(false);
    const cardsColumnListHistory = JSON.parse(JSON.stringify(columnsCardsHistory)) // deep copy
    setColumnsCards(cardsColumnListHistory);
    setDraggedAndMovedItems([])
  };
  
  const applySettingsHandler = function (e) {
    setOpenSettings(false);
    setDisabled(false);
    const colCards = JSON.parse(JSON.stringify(columnsCards))
    const propsCols = JSON.parse(JSON.stringify(props.colNames));
    colCards.forEach(e => {
      const fi = propsCols.findIndex(th => th.colshort.trim().toLowerCase() === e.colshort.trim().toLowerCase())
      propsCols[fi].isVisible = e.selected
    })

    const draggableElements = document.querySelectorAll('.draggable')
    draggableElements.forEach((el,i) => {
      const fi = propsCols.findIndex(th => th.colshort.trim().toLowerCase() === el.dataset.shortname.trim().toLowerCase())
      propsCols[fi].order = i
    })
   

    props.setTableHeadings(propsCols)
    props.settingsApplied(true)

  };
  

 
  const handleCheck = (isChecked, index) => {
    const columnCardsList = [...columnsCards];
    columnCardsList[index].selected = isChecked;
    setColumnsCards(columnCardsList);
  };



  /*********************** Drag and positioning columns cards *******************************/
  const cardDragStartHandler = (e) => {
    e.target.classList.add('dragging')
  }

  const cardDragEndHandler = (e) => {
    e.target.classList.remove('dragging')
  }

  const containerDragOverHandler = (e) => {
     e.preventDefault()
    const container = e.currentTarget;
     const afterElement = getDragAfterElementXaxis(container, e.clientX)
     const draggable = document.querySelector('.dragging')
    
     if(container)
     {
       if (afterElement == null) {
         if(draggable)
          {
            container.appendChild(draggable)
          }
        } 
        else 
        {
          if(draggable)
            {
              container.insertBefore(draggable, afterElement)
              const draggable_index = Number(draggable.id.split('_')[1])
              const afterElement_index = Number(afterElement.id.split('_')[1])
              const arr = JSON.parse(JSON.stringify(draggedAndMovedItems))
              arr.push({
                name : `from ${draggable.id} to ${afterElement.id}`,
                from_index : draggable_index,
                to_index : afterElement_index
              })
              setDraggedAndMovedItems(arr)
            }
        }
    }
    
  }
  
  const getDragAfterElementXaxis = (container, x) => {
    const draggableElements = [...container.querySelectorAll('.draggable:not(.dragging)')]
    return draggableElements.reduce((closest, child) => {
      const box = child.getBoundingClientRect()
      const offset = x - box.left - box.width / 2
      if (offset < 0 && offset > closest.offset) {
        return { offset: offset, element: child }
      } else {
        return closest
      }
    }, { offset: Number.NEGATIVE_INFINITY }).element
  }
  /*********************** EOF:: Drag and positioning columns cards *******************************/

 

  return (
    <>
      <div className="tb__upper">
            <button
              className="settings__btn cu__pnt"
              onClick={tableSettingsHandler}
              disabled={isDisabled}
            >
              <FontAwesomeIcon
                icon={faSlidersH}
                className="slider__icon sb__ch"
              />
              <h4 className="sb__txt">Settings</h4>
            </button>
      </div>
      <div
        className={`settings__wrapper ${openSettings ?  '' : 'hide'}`}
      >
        <div className="dm__heading">
        <h3>Dimensions and Metrics</h3>
        </div>
        <div 
        className="col__names__drag wrapper_container" 
        onDragOver={containerDragOverHandler}
      

      >
          {
          columnsCards.map((eobj,i) => {
              return (
                  
                  <label
                  draggable
                  onDragStart={cardDragStartHandler}
                  onDragEnd={cardDragEndHandler}
                  className="col__names__card draggable" 
                  key={`col_${i}`}
                  id={`col_${i}`}
                  data-order={eobj.order}
                  data-shortname={eobj.colshort}
                  >
                   <input type="checkbox" 
                    name="col__selection__name" 
                    value={eobj.colname} 
                    className='default__element'
                    checked={eobj.selected}
                    onChange={(e) => {
                      handleCheck(e.target.checked,i);
                    }}
                    />
                   <div className='col__option__name'>{eobj.colname}</div>
                 </label>
              );
          })
        }
        </div>


        <div className="btn__func">
        <button
          className="settings__btn__group close__btn center__flex cu__pnt"
          onClick={closeSettingsHandler}
        >
          Close
        </button>
        <button className="settings__btn__group apply__changes__btn center__flex cu__pnt"
          onClick={applySettingsHandler}
        >
          Apply Changes
        </button>
        </div>
      </div>
    </>
  );
}

export default Settings;
