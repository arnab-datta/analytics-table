import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFilter } from "@fortawesome/free-solid-svg-icons";
import moment from "moment";
import "../../styles/css/Table.css";
import { useEffect, useState } from "react";
import apps_icon from "../../assets/images/apps_icon.png";
import NotFoundData from "../NotFoundData/NotFoundData";
import Filter from "../Filter/Filter";


function Table(props) {

  const [TableMainData, setTableMainData] = useState([]);
  const [TableData, setTableData] = useState([]);
  const [allApps, setAllApps] = useState([]);
  const [tableHeadingsVal, setTableHeadingsVal] = useState([]);
  const [tableTopRowData, setTableTopRowData] = useState([]);
  const [dataFound, setDataFound] = useState(false)
  const [openFilter, setOpenFilter] = useState(false)
  const [filterValues, setFilterValues] = useState([])
  const [AllValues, setAllValues] = useState([])




  useEffect(() => {
    const newTableHeadings = JSON.parse(JSON.stringify(props.TableHeadings)) // deep copy
    const tableHeadingsComp = JSON.parse(JSON.stringify(tableHeadingsVal)) // deep copy

    newTableHeadings.sort((a, b) => a.order > b.order ? 1 : -1) // ascending

    newTableHeadings.forEach(el => {
      const thfin = tableHeadingsComp.findIndex(th => th.colshort.trim().toLowerCase() === el.colshort.trim().toLowerCase())
      if(thfin > -1)
      {
        el.fieldAllValues = tableHeadingsComp[thfin].fieldAllValues
        el.fieldFilterValues = tableHeadingsComp[thfin].fieldFilterValues
      }
    })

    const tableData = JSON.parse(JSON.stringify(TableData)) // deep copy
    if (tableData.length > 0) {
      tableData.forEach((td, i) => {
        td.forEach((cell, j) => {
          
            const thfi = newTableHeadings.findIndex(th => th.colshort.trim().toLowerCase() === cell.colshort.trim().toLowerCase())
            if (thfi > -1) {
              cell.order = newTableHeadings[thfi].order
              cell.isVisible = newTableHeadings[thfi].isVisible
            }
          
        })
        td.sort((a, b) => a.order > b.order ? 1 : -1) // ascending
      })


      const tableTopRowDataUpdated = JSON.parse(JSON.stringify(tableTopRowData)) // deep copy
      tableTopRowDataUpdated.forEach((cell, j) => {
        
          const thfi = newTableHeadings.findIndex(th => th.colshort.trim().toLowerCase() === cell.colshort.trim().toLowerCase())
          if (thfi > -1) {
            cell.order = newTableHeadings[thfi].order
            cell.isVisible = newTableHeadings[thfi].isVisible
          }
        
      })
      tableTopRowDataUpdated.sort((a, b) => a.order > b.order ? 1 : -1) // ascending
      setTableHeadingsVal(JSON.parse(JSON.stringify(newTableHeadings)));
      setTableTopRowData(tableTopRowDataUpdated)
      setTableData(tableData);
    }
  }, [props.TableHeadings])

  useEffect(() => {
    const API_CALLS = Promise.all([
      fetch(
        `http://go-dev.greedygame.com/v3/dummy/report?startDate=${props.startDateSelected}&endDate=${props.endDateSelected}`,
        { method: "GET" }
      ),
      fetch(`http://go-dev.greedygame.com/v3/dummy/apps`, { method: "GET" }),
    ]);

    API_CALLS.then((results) => Promise.all(results.map((r) => r.json())))
      .then((results) => {
        try {
          let resReport = results[0];
          let resAllApps = results[1];
          if (results[0].error !== "invalid request") {
            const respTableData = resReport.data.map((td) => {
              td.app_name = resAllApps.data.filter(
                (app) => app.app_id === td.app_id
              )[0].app_name;
              return td;
            })
            respTableData.forEach((obj, i) => {
              obj.date = moment(obj.date).format("MMM DD YYYY")
              obj.revenue = Number(obj.revenue).toFixed(2)
              obj.fillrate = ((Number(obj.requests) / Number(obj.responses)) * 100).toFixed(2)
              obj.ctr = ((Number(obj.clicks) / Number(obj.impressions)) * 100).toFixed(2)
            })
            const tableHeadings = JSON.parse(JSON.stringify(props.TableHeadings)) // deep copy
            tableHeadings.sort((a, b) => a.order > b.order ? 1 : -1); // ascending
            tableHeadings.forEach(el => {
              el.filterShow = false
              el.fieldAllValues = []
              el.fieldFilterValues = []
            })
            const tableData = []
            respTableData.forEach((elob, i) => {
              const rowData = []
              for (let property in elob) {
                const thfi = tableHeadings.findIndex(th => th.colshort.trim().toLowerCase() === property.trim().toLowerCase())
                if (thfi > -1) {
                  const col_obj = {
                    order: tableHeadings[thfi].order,
                    colshort: property,
                    colname: tableHeadings[thfi].colname,
                    isVisible: tableHeadings[thfi].isVisible,
                    value: elob[property]
                  }
                  rowData.push(col_obj)
                }
              }
              rowData.sort((a, b) => a.order > b.order ? 1 : -1); // ascending
              tableData.push(rowData)
            })



            const tableTopRowData = []
            const newTableHeadings = JSON.parse(JSON.stringify(props.TableHeadings))
            const sd = JSON.parse(JSON.stringify(props.startDateSelected))
            const ed = JSON.parse(JSON.stringify(props.endDateSelected))
            newTableHeadings.forEach((elob, i) => {

              let val = 0;
              const cls = elob.colshort.trim().toLowerCase();
              if (cls === "date") {
                val = dateDiffInDays(new Date(sd), new Date(ed));
              }
              else if (cls === "app_name") {
                const appsArr = resAllApps.data.filter((v, i, a) => a.indexOf(v) === i);
                val = appsArr.length
              }
              else {
                val = convertToInternationalSystem(respTableData.reduce((acc, obj) => { return acc + Number(obj[cls]) }, 0));
              }
              const col_obj = {
                order: elob.order,
                colshort: elob.colshort,
                colname: `total_${elob.colname}`,
                isVisible: elob.isVisible,
                value: val,
              }
              tableTopRowData.push(col_obj)
            })

            tableTopRowData.sort((a, b) => a.order > b.order ? 1 : -1); // ascending
            setTableTopRowData(tableTopRowData)
            setTableHeadingsVal(JSON.parse(JSON.stringify(tableHeadings)));
            setTableData(tableData);
            setTableMainData(tableData);
            setAllApps(resAllApps.data);
            props.setIsLoading(false);
            props.setRenderSettings(true);
            setDataFound(true)


          }
          else if (!resReport.hasOwnProperty("data")) {
            props.setRenderSettings(false);
            setDataFound(false)
          }
        } catch (ex) {
          props.setRenderSettings(false);
          setDataFound(false)
          throw ex;
        }
      })
      .catch((err) => {
        // alert('some error occurred')
        props.setRenderSettings(false);
        setDataFound(false)
      });
  }, [props.startDateSelected, props.endDateSelected]);


  const convertToInternationalSystem = (value) => {

    // Nine Zeroes for Billions
    return Math.abs(Number(value)) >= 1.0e+9

      ? (Math.abs(Number(value)) / 1.0e+9).toFixed(2) + "B"
      // Six Zeroes for Millions 
      : Math.abs(Number(value)) >= 1.0e+6

        ? (Math.abs(Number(value)) / 1.0e+6).toFixed(2) + "M"
        // Three Zeroes for Thousands
        : Math.abs(Number(value)) >= 1.0e+3

          ? (Math.abs(Number(value)) / 1.0e+3).toFixed(2) + "K"

          : Math.abs(Number(value)).toFixed(2);

  }

  const openFilterHandler = (ev) => {
    const _this = ev.currentTarget;
    const tableHeadings = JSON.parse(JSON.stringify(tableHeadingsVal))
    const tableData = JSON.parse(JSON.stringify(TableData)) // deep copy
    const col_short = _this.id.split('-')[1].trim().toLowerCase();

    const fTableH = tableHeadings.findIndex(el => el.colshort.trim().toLowerCase() === col_short)
    if(fTableH > - 1)
    {
      tableHeadings[fTableH].filterShow = true;
      // setTableHeadingsVal(JSON.parse(JSON.stringify(tableHeadings)));
    }

    switch(_this.dataset.ftype.trim().toLowerCase())
    {
      case "number" : 
          {
            
            if(tableHeadings[fTableH].fieldFilterValues.length > 0)
            {
              setAllValues(JSON.parse(JSON.stringify(tableHeadings[fTableH].fieldAllValues)))
              setFilterValues(JSON.parse(JSON.stringify(tableHeadings[fTableH].fieldFilterValues)))
            }
            else
            {
              let valueArray = [];
              tableData.forEach((row,i) => {
                let fi = row.findIndex(col => col.colshort.trim().toLowerCase() === col_short)
                if(fi>-1){
                    valueArray.push(row[fi].value)
                }
              })
              valueArray.sort()
              const arr = [valueArray[0],valueArray[valueArray.length - 1]]
              tableHeadings[fTableH].fieldAllValues = JSON.parse(JSON.stringify(arr))
              tableHeadings[fTableH].fieldFilterValues = JSON.parse(JSON.stringify(arr))
              setFilterValues(JSON.parse(JSON.stringify(arr)))
              setAllValues(JSON.parse(JSON.stringify(arr)))
            }
            break;
          }
      case "string" : 
            {
              
              if(tableHeadings[fTableH].fieldFilterValues.length > 0)
              {
                setAllValues(JSON.parse(JSON.stringify(tableHeadings[fTableH].fieldAllValues)))
                setFilterValues(JSON.parse(JSON.stringify(tableHeadings[fTableH].fieldFilterValues)))
              }
              else
              {
                let valueArray = [];
                tableData.forEach((row,i) => {
                  let fi = row.findIndex(col => col.colshort.trim().toLowerCase() === col_short)
                  if(fi>-1){
                      valueArray.push(row[fi].value)
                  }
                })
                valueArray = valueArray.filter((v, i, a) => a.indexOf(v) === i);
                valueArray.sort();
                tableHeadings[fTableH].fieldAllValues = JSON.parse(JSON.stringify(valueArray))
                setAllValues(JSON.parse(JSON.stringify(valueArray)))
                tableHeadings[fTableH].fieldFilterValues = []
                setFilterValues(JSON.parse(JSON.stringify([])))
              }
              break;
            }
      case "date" : 
            {
              if(tableHeadings[fTableH].fieldFilterValues.length > 0)
              {
                setAllValues(JSON.parse(JSON.stringify(tableHeadings[fTableH].fieldAllValues)))
                setFilterValues(JSON.parse(JSON.stringify(tableHeadings[fTableH].fieldFilterValues)))
              }
              else
              {
                const std = JSON.parse(JSON.stringify(props.startDateSelected))
                const edd = JSON.parse(JSON.stringify(props.endDateSelected))
                const arr = [std,edd]
                tableHeadings[fTableH].fieldAllValues = arr
                setAllValues(arr)
                tableHeadings[fTableH].fieldFilterValues = []
                setFilterValues([])
              }
              break;
            }
      
    }
    setTableHeadingsVal(JSON.parse(JSON.stringify(tableHeadings)));
    setOpenFilter(true)
  }

  return (
    <>
      {dataFound
        ?
        <div className="table__parent__wrapper">
          {(TableData.length > 0 &&
            <div>

              <table id="" className="table__wrapper">
                <thead>
                  <tr className="d__flex">
                    {tableHeadingsVal.map((el, index) => {
                      return (
                        <th
                          key={`${el.colname}_${index}_head`}
                          className={`d__flex flex__dir__col cu__pnt 
                              ${el.order === 0 || el.order === 1 ? "al__it__left" : "al__it__right"}
                              ${el.isVisible ? '' : 'hide'}
                        `}
                        >
                          <span
                             className={`filter__icon__wrapper
                                ${el.order === 0 || el.order === 1 ? 'txt__alg__lft' : 'txt__alg__rgt'} 
                             `}
                             id={`filter-${el.colshort}`}
                             onClick={openFilterHandler}
                             data-ftype={el.fieldType}
                          >
                            <FontAwesomeIcon
                              icon={faFilter}
                              className="filter__icon"
                            />
                          </span>
                          {(el.filterShow &&
                              <Filter
                              col_obj={el}
                              openFilter={openFilter}
                              TableData={TableData}
                              setTableData={setTableData}
                              TableMainData={TableMainData}
                              tableHeadingsVal={tableHeadingsVal}
                              setTableHeadingsVal={setTableHeadingsVal}
                              filterValues={filterValues}
                              AllValues={AllValues}
                              pos_lft={el.order !== 8 && el.order !== 7 && el.order !== 0 && el.order !== 1 ? "86%" : (el.order === 0 && el.order === 1 ? "5%" : "")}
                              pos_rgt={el.order === 8 ? "5%" : (el.order === 7 ? "10%" : "")}
                              ></Filter>
                            )
                          }
                          <span className="theader__txt">{el.colname}</span>
                        </th>
                      );
                    })}
                  </tr>
                </thead>
                <tbody>

                  <tr className="d__flex">
                    {
                      tableTopRowData.map((obj, index) => {
                        return (
                          <td
                            key={`td_${index}_top`}
                            className={`table__data__top
                                ${obj.order === 0 || obj.order === 1 ? 'txt__alg__lft' : 'txt__alg__rgt'} 
                                ${obj.isVisible ? '' : 'hide'}`
                            }
                          >
                            {obj.value}
                          </td>
                        );
                      })
                    }
                  </tr>
                  {TableData.map((datarow, i) => {

                    return (

                      <tr className="d__flex" key={`tr_r_${i}`}>
                        {
                          datarow.map((cell, j) => {
                            return (
                              <td
                                key={`td_d_${j}`}
                                className={`table__data
                                    ${cell.order === 0 || cell.order === 1 ? 'txt__alg__lft' : 'txt__alg__rgt'} 
                                    ${cell.isVisible ? '' : 'hide'}
                                    ${cell.colshort === "app_name" ? 'app__name__cls' : ''}`
                                }
                              >
                                {cell.colshort === "app_name"
                                  ? (<>
                                    <div className="img__app__name">
                                      <img src={apps_icon} alt="app_name" />
                                    </div>
                                    <span>{cell.value}</span>
                                  </>
                                  )
                                  : cell.colshort === "fillrate" || cell.colshort === "ctr"
                                    ? (<>{cell.value}%</>)
                                    : (<>{cell.value}</>)

                                }

                              </td>
                            );
                          })
                        }

                      </tr>
                    );
                  })
                  }
                </tbody>
              </table>

            </div>
          )}
        </div>
        :
        <NotFoundData></NotFoundData>
      }
    </>
  );
}
export default Table;


const _MS_PER_DAY = 1000 * 60 * 60 * 24;
const dateDiffInDays = (a, b) => {
  // Discard the time and time-zone information.
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}
