import { useState } from "react";
import Settings from "../Settings/Settings";

import Table from "../Table/Table";


function TableGroup(props) {
   
    const [colNames, setColNames] = useState([])
    const [renderSettings, setRenderSettings] = useState(false);
    const [settingsApplyState, settingsApplied] = useState(false);
    const [TableHeadings,setTableHeadings] = useState([
        { order:0, colshort: "date" , colname : "Date", isVisible : true, fieldType: "date" },
        { order:1, colshort: "app_name" , colname : "App Name", isVisible : true, fieldType: "string" },
        { order:2, colshort: "requests" , colname : "AD Request", isVisible : true, fieldType: "number" },
        { order:3, colshort: "responses" , colname : "AD Response", isVisible : true, fieldType: "number" },
        { order:4, colshort: "impressions" , colname : "Impression", isVisible : true, fieldType: "number" },
        { order:5, colshort: "clicks" , colname : "Clicks", isVisible : true, fieldType: "number" },
        { order:6, colshort: "revenue" , colname : "Revenue", isVisible : true, fieldType: "number" },
        { order:7, colshort: "fillrate" , colname : "Fill Rate", isVisible : true, fieldType: "number" },
        { order:8, colshort: "ctr" , colname : "CTR", isVisible : true, fieldType: "number" }
    ]);
    const [columnsSync,setColumnSync] = useState([]);
    return(
    <>
    
        {renderSettings && (
            <Settings
              colNames={TableHeadings}
              settingsApplied={settingsApplied}
              renderSettings={renderSettings}
              setColumnSync={setColumnSync}
              setTableHeadings={setTableHeadings}
            ></Settings> 
        )}
          
        
     
            <Table
              startDateSelected={props.startDateSelected}
              endDateSelected={props.endDateSelected}
              setIsLoading={props.setIsLoading}
              settingsApplyState={settingsApplyState}
              setRenderSettings={setRenderSettings}
              TableHeadings={TableHeadings}
            
            ></Table>
          
     
        
        
    </>
)};

export default TableGroup