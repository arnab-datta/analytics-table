import { useState } from "react";
import DatePicker from "../../Components/DatePicker/DatePicker";
import Loader from "../../Components/Loader/Loader";
import TableGroup from "../../Components/TableGroup/TableGroup";
import "../../styles/css/Analytics.css";


function Analytics() {
  const [startDateSelected, setStartDateSelected] = useState("");
  const [endDateSelected, setEndDateSelected] = useState("");
  const [isLoading, setIsLoading] = useState(true);


  return (
    <div>
      <div className="wrapper__analytics d__flex">
        <div className="leftSection"></div>
        <div className="rightSection d__flex flex__dir__col scroll__cls">
          <h1>Analytics</h1>
          
          <DatePicker
            startDateSelected={startDateSelected}
            endDateSelected={endDateSelected}
            setStartDateSelected={setStartDateSelected}
            setEndDateSelected={setEndDateSelected}
            setIsLoading={setIsLoading}
           
          ></DatePicker>

          {isLoading && <Loader></Loader>}
          
          {!isLoading && 
          (
             
                  <TableGroup
                  startDateSelected={startDateSelected}
                  endDateSelected={endDateSelected}
                  setIsLoading={setIsLoading}
                  
                  ></TableGroup>
             
              
            )
          }
          

        </div>
      </div>
    </div>
  );
}

export default Analytics;
